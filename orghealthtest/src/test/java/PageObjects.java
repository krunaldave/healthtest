import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;

public class PageObjects
{
	public PageObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// FOOD ITEM TO BE SELECTED

	@CacheLookup @AndroidFindBy(xpath = "//android.widget.TextView[@text='Fuel your body']")
	public AndroidElement foodObjective;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/btn_login") public AndroidElement baseLoginButton;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/et_username") public AndroidElement emailId;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/et_password") public AndroidElement password;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/btn_login_signup") public AndroidElement loginSignup;

	@CacheLookup @AndroidFindBy(id = "android:id/button1") public AndroidElement popUpCancel;

	@CacheLookup @AndroidFindBy(uiAutomator = "text(\"Me\")") public AndroidElement meBtn;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/breakfast_layout") public AndroidElement breakFast;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/et_search") public AndroidElement searchItem;

	@CacheLookup @AndroidFindBy(xpath = "//android.widget.TextView[@text='Tea with Milk']") public AndroidElement item;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/btn_add") public AndroidElement addItem;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/btn_done") public AndroidElement doneBtn;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/tv_left") public AndroidElement tvLeft;

	@CacheLookup @AndroidFindBy(id = "com.healthifyme.basic:id/iv_badge_profile_pic") public AndroidElement profilePic;
}
