import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseCapabilities
{

	public static AndroidDriver<AndroidElement> andDriver;

	public static AndroidDriver<AndroidElement> getAndroidSetup() throws MalformedURLException {

		String url = "http://127.0.0.1:4723/wd/hub";
		String appPkg = "com.healthifyme.basic";
		String appActivity = "com.healthifyme.basic.activities.LaunchActivity";

		DesiredCapabilities dCap = new DesiredCapabilities();
		dCap.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus 5");
		dCap.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
		dCap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPkg);
		dCap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appActivity);
		dCap.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.APPIUM);
		dCap.setCapability(MobileCapabilityType.NO_RESET, "true");
		andDriver = new AndroidDriver(new URL(url), dCap);

		return andDriver;
	}
}
