import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.pmw.tinylog.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TestCase extends BaseCapabilities
{
	public static AndroidDriver<AndroidElement> andDriver;
	public static PageObjects po;

	@BeforeClass public void getDriver() throws MalformedURLException {
		andDriver = BaseCapabilities.getAndroidSetup();
		po = new PageObjects(andDriver);
		Logger.info("Starting app");
		andDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 0) public void login() {
		String emailId = "hme-testpr160@example.com";
		String password = "password";

		Assert.assertTrue(andDriver.currentActivity().contains("NewLoginSignupActivity"));
		Assert.assertNotNull(po.baseLoginButton);
		po.baseLoginButton.click();
		Assert.assertNotNull(po.emailId);
		po.emailId.sendKeys(emailId);
		Assert.assertNotNull(po.password);
		po.password.sendKeys(password);
		Assert.assertNotNull(po.loginSignup);
		po.loginSignup.click();
		andDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 1) public void goToProfile() {
		Assert.assertNotNull(po.popUpCancel);
		po.popUpCancel.click();

		Assert.assertTrue(andDriver.currentActivity().contains("DashboardActivity"));
		Assert.assertNotNull(po.meBtn);
		po.meBtn.click();

		/*--- Scrolling tried with various methods, None of them worked
		 1. TouchActions with coordinates.
		 2. UIAutomator
		 Finally implemented swipe method from element to element to make it scroll ---*/

		Assert.assertNotNull(po.tvLeft);
		Assert.assertNotNull(po.profilePic);
		new TouchAction(andDriver).press(ElementOption.element(po.tvLeft))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1))).moveTo(ElementOption.element(po.profilePic))
				.release().perform();

		Assert.assertNotNull(po.foodObjective);
		po.foodObjective.click();
		Assert.assertNotNull(po.breakFast);
		po.breakFast.click();
	}

	@Test(priority = 2) public void selectMeal() {
		String foodItem = "Milk";
		Assert.assertNotNull(po.searchItem);
		po.searchItem.sendKeys(foodItem);
		Assert.assertTrue(andDriver.currentActivity().contains("NutritionSearchActivity"));
		Assert.assertNotNull(po.item);
		po.item.click();

		Assert.assertTrue(andDriver.currentActivity().contains("QuantityPickerActivity"));
		Assert.assertNotNull(po.addItem);
		po.addItem.click();

		Assert.assertTrue(andDriver.currentActivity().contains("FoodTrackSummaryActivity"));
		Assert.assertNotNull(po.doneBtn);
		po.doneBtn.click();

		Assert.assertNotNull(po.meBtn);
		po.meBtn.click();
		Assert.assertTrue(andDriver.currentActivity().contains("DashboardActivity"));
	}

	@AfterClass public void tearDown() {
		Logger.info("Closing app");
	}
}
